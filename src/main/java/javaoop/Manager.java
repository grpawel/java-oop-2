package javaoop;

import java.util.ArrayList;
import java.util.prefs.BackingStoreException;

/**
 * Created by Paweł Grochola on 09.04.2016.
 */
public class Manager extends Employee {
    ArrayList<Employee> employees;

    public Manager(String name, String surname, int salary) {
        super(name, surname, salary);
        employees = new ArrayList<Employee>();
    }
    public Boolean canHireEmployee(Employee candidate) {
        return true;
    }

    public Boolean hireEmployee(Employee candidate) {
        if(!canHireEmployee(candidate)) {
            return false;
        }
        employees.add(candidate);
        return true;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(this.name);
        sb.append(" - Manager");
        for (Employee employee : employees) {
            sb.append("\n");
            sb.append(employee.toString());
        }
        //if some employees happen to be managers and employ people,
        //this will indent all of them
        return sb.toString().replaceAll("\n", "\n    ");
    }
}
