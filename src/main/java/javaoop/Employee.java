package javaoop;

/**
 * Created by Paweł Grochola on 08.04.2016.
 */
public class Employee {
    protected final String name;
    protected final String surname;
    protected int salary;

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public int getSalary() {

        return salary;
    }

    public void sayName() {
        System.out.println("I am " + name + " " + surname + ".");
    }

    public void saySalary() {
        System.out.println("My salary is " + salary + ".");
    }

    public boolean isSatisfied() {
        return salary > 10000;
    }

    public void saySatisfaction() {
        if(isSatisfied()) {
            System.out.println("I am satisfied.");
        }
        else {
            System.out.println("I am not satisfied.");
        }
    }

    public Employee(String name, String surname, int salary) {
        this.name = name;
        this.surname = surname;
        this.salary = salary;
    }

    @Override
    public String toString() {
        return this.name + " - Employee";
    }
}
