package javaoop;

/**
 * Created by Paweł Grochola on 09.04.2016.
 */
public class Company {
    protected Manager ceo = null;
    private static Company instance = null;

    private Company() {}

    public static Company getInstance() {
        if(instance == null) {
            instance = new Company();
        }
        return instance;
    }

    Boolean hireCeo(Manager ceo) {
        this.ceo = ceo;
        return true;
    }

    @Override
    public String toString() {
        return ceo.toString().replaceFirst("Manager", "CEO");
    }
}
