package javaoop;

/**
 * Created by Paweł Grochola on 09.04.2016.
 */
public class ManagerFixedTeamSize extends Manager {
    protected final int employeeLimit;
    protected int employeeLimitLeft;


    public ManagerFixedTeamSize(String name, String surname, int salary, int employeeLimit) {
        super(name, surname, salary);
        this.employeeLimit = employeeLimit;
        this.employeeLimitLeft = employeeLimit;
    }

    @Override
    public Boolean canHireEmployee(Employee candidate) {
        if(employeeLimitLeft <= 0) {
            return false;
        }
        return super.canHireEmployee(candidate);
    }

    @Override
    public Boolean hireEmployee(Employee candidate) {
        if(!canHireEmployee(candidate)) {
            return false;
        }
        employeeLimitLeft -= 1;
        employees.add(candidate);
        return true;
    }
}
