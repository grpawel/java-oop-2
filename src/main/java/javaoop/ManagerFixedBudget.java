package javaoop;

/**
 * Created by Paweł Grochola on 09.04.2016.
 */
public class ManagerFixedBudget extends Manager {
    protected final int budget;
    protected int budgetLeft;

    public ManagerFixedBudget(String name, String surname, int salary, int budget) {
        super(name, surname, salary);
        this.budget = budget;
        this.budgetLeft = budget;
    }

    @Override
    public Boolean canHireEmployee(Employee candidate) {
        int salary = candidate.getSalary();
        if(budgetLeft < salary) {
            return false;
        }
        return super.canHireEmployee(candidate);
    }

    @Override
    public Boolean hireEmployee(Employee candidate) {
        if(!canHireEmployee(candidate)) {
            return false;
        }
        budgetLeft -= candidate.getSalary();
        employees.add(candidate);
        return true;
    }

}
