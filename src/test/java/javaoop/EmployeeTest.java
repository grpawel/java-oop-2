package javaoop;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.junit.Assert.*;

/**
 * Created by Paweł Grochola on 08.04.2016.
 */
public class EmployeeTest {
    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private final ByteArrayOutputStream errContent = new ByteArrayOutputStream();

    private Employee employee;
    private Employee badEmployee;

    @Before
    public void setUpStreams() throws Exception {
        System.setOut(new PrintStream(outContent));
        System.setErr(new PrintStream(errContent));
    }

    @Before
    public void setUpEmployee() throws Exception {
        employee = new Employee("John", "Good", 15000);
        badEmployee = new Employee("Dee", "Dee", 8000);
    }

    @After
    public void cleanupStreams() throws Exception {
        System.setOut(null);
        System.setErr(null);
    }


    @Test
    public void testSayName() throws Exception {
        employee.sayName();
        assertEquals("I am John Good.", outContent.toString().replaceAll("(\\r|\\n)", ""));
    }

    @Test
    public void testSaySalary() throws Exception {
        employee.saySalary();
        assertEquals("My salary is 15000.", outContent.toString().replaceAll("(\\r|\\n)", ""));
    }

    @Test
    public void testIsSatisfied() throws Exception {
        assertTrue(employee.isSatisfied());
        assertFalse(badEmployee.isSatisfied());
    }

    @Test
    public void testSaySatisfied() throws Exception {
        employee.saySatisfaction();
        assertEquals("I am satisfied.", outContent.toString().replaceAll("(\\r|\\n)", ""));
    }

    @Test
    public void testSayNotSatisfied() throws Exception {
        badEmployee.saySatisfaction();
        assertEquals("I am not satisfied.", outContent.toString().replaceAll("(\\r|\\n)", ""));
    }
}