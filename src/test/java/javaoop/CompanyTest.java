package javaoop;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Created by Paweł Grochola on 09.04.2016.
 */
public class CompanyTest {
    private Company company;
    private Company company2;

    private Employee employee = mock(Employee.class);

    @Before
    public void setUp() throws Exception{
        company = Company.getInstance();
        company2 = Company.getInstance();
        setUpStructure();
    }

    public void setUpStructure() throws Exception {
        Manager ceo = new Manager("Zbyszek", "rrr", 10000);
        Manager maciek = new Manager("Maciek", "eee", 2000);
        Manager jacek = new Manager("Jacek", "grg", 8945);
        Employee wacek = new Employee("Wacek", "eee", 2030);
        Employee grzesiek = new Employee("Grzesiek", "efef", 3093);

        company.hireCeo(ceo);
        ceo.hireEmployee(maciek);
        maciek.hireEmployee(jacek);
        maciek.hireEmployee(wacek);
        ceo.hireEmployee(grzesiek);
    }

    @Test
    public void testUnique() throws Exception {
        assertTrue(company == company2);
    }

    @Test
    public void testToString() throws Exception {

        String expected =
                        "Zbyszek - CEO\n" +
                        "    Maciek - Manager\n" +
                        "        Jacek - Manager\n" +
                        "        Wacek - Employee\n" +
                        "    Grzesiek - Employee";
        String result = company.toString();
        assertEquals(expected, result);
    }
}