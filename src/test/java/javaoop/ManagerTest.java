package javaoop;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;


/**
 * Created by Paweł Grochola on 09.04.2016.
 */
public class ManagerTest {
    private Manager manager;
    private Manager manager2;

    @Before
    public void setUp() throws Exception {
        manager = new Manager("A", "AA", 10000);
        Employee employee1 = new Employee("B", "BB", 10000);
        Employee employee2 = new Employee("C", "CC", 10000);
        manager.hireEmployee(employee1);
        manager.hireEmployee(employee2);

        manager2 = new Manager("Z", "efe", 3393);
    }

    @Test
    public void testToString() throws Exception {
        String expected =
                "A - Manager\n" +
                "    B - Employee\n" +
                "    C - Employee";
        String result = manager.toString();
        assertEquals(expected, result);
    }

    @Test
    public void testToStringNoEmployees() throws Exception {
        String expected = "Z - Manager";
        String result = manager2.toString();
        assertEquals(expected, result);
    }
}