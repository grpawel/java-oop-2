package javaoop;

import com.sun.org.apache.xpath.internal.operations.Bool;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;

/**
 * Created by Paweł Grochola on 09.04.2016.
 */
public class ManagerFixedTeamSizeTest {
    private ManagerFixedTeamSize manager;
    private Employee employee = mock(Employee.class);
    private Employee employee2 = mock(Employee.class);

    @Before
    public void setUp() throws Exception {
        manager = new ManagerFixedTeamSize("A", "AA", 10000, 1);
    }

    @Test
    public void testCanHireOneEmployee() throws Exception {
        Boolean result = manager.canHireEmployee(employee);
        assertTrue(result);
    }

    @Test
    public void testCanHireEmployees() throws Exception {
        manager.hireEmployee(employee);
        Boolean result = manager.canHireEmployee(employee2);
        assertFalse(result);
    }

    @Test
    public void testHireOneEmployee() throws Exception {
        Boolean result = manager.hireEmployee(employee);
        assertTrue(result);
    }
    @Test
    public void testHireTwoEmployees() throws Exception {
        manager.hireEmployee(employee);
        Boolean result = manager.canHireEmployee(employee2);
        assertFalse(result);
    }

}