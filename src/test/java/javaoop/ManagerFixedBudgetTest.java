package javaoop;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Created by Paweł Grochola on 09.04.2016.
 */
public class ManagerFixedBudgetTest {
    private ManagerFixedBudget manager;
    private Employee cheapEmployee = mock(Employee.class);
    private Employee expensiveEmployee = mock(Employee.class);
    private Employee employee = mock(Employee.class);

    @Before
    public void setUp() throws Exception {
        manager = new ManagerFixedBudget("A", "AA", 10000, 15000);
    }

    @Test
    public void testCanHireCheapEmployee() throws Exception {
        when(employee.getSalary()).thenReturn(5000);
        Boolean result = manager.canHireEmployee(employee);
        assertTrue(result);
    }

    @Test
    public void testCanHireExpensiveEmployee() throws Exception {
        when(employee.getSalary()).thenReturn(20000);
        Boolean result = manager.canHireEmployee(employee);
        assertFalse(result);
    }

    @Test
    public void testHireCheapEmployee() throws Exception {
        when(employee.getSalary()).thenReturn(5000);
        Boolean result = manager.hireEmployee(employee);
        assertTrue(result);
    }

    @Test
    public void testHireExpensiveEmployee() throws Exception {
        when(employee.getSalary()).thenReturn(20000);
        Boolean result = manager.hireEmployee(employee);
        assertFalse(result);
    }
}